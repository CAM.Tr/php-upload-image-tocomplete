<?php
// Connexion à la base de données
try {
    // TODO mettre les informations de connexion
    $bdd = new PDO(?????, ?????, ?????);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

// 
/**
 * Fonction qui vérifie qu'un enregistrement n'est pas déjà en Base.
 * @return 1 si     
 */
function verifier_doublon(string $titre, string $message, string $fichier, $connexion): int {

    // TODO faire requête pour 
    $req = "SELECT * FROM annonces WHERE titre= :titre AND message= :message AND nom_image = :nom_image";

    $reponse = $connexion->prepare($req);
    $reponse->bindValue(":titre", $titre, PDO::PARAM_STR);
    $reponse->bindValue(":message", $message, PDO::PARAM_STR);
    $reponse->bindValue(":nom_image", $fichier, PDO::PARAM_STR);
    $reponse->execute();

    return $reponse->rowCount();

}

// vérifier si le fichier est uploadé (faire un "var_dump" de $_FILE pour voir ce que ça contient)
if (???????) {

    $erreurs = array();

    $dossier = getcwd() . "\img\\";
    // récupération du nom de fichier
    $fichier = basename($_FILES['image']['name']);

    // TODO vérifier les extensions
    $extensions = array('.png', '.gif', '.jpg', '.jpeg', '.JPG', '.JPEG', '.PNG', '.GIF');
    $extension = strrchr($_FILES['image']['name'], '.');

    if (???????)
    {
        //Si l'extension n'est pas dans le tableau
        $erreurs["image"] = 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg...';
    } else {

        echo "Taille du fichier image : ".$_FILES['image']['size']." Octets<br>";

        // TODO vérifier la taille du fichier, limiter à 20 mo
        if ($_FILES['image']['size'] > ???????) {
            $erreurs["image"] = 'Votre fichier image est trop volumineux !';
        } else {

            // on déplace le fichier dans le dossier "image"
            if (move_uploaded_file($_FILES['image']['tmp_name'], $dossier . $fichier)) {
                echo 'Upload effectué avec succès !';

                // TODO vérifier si il y a un doublon (voir à utiliser la fonction "vérifier_doublon")
                if(??????) {
                    header("location:index.php");
                }else{
                    // TODO si pas de doublon, insertion en base de données
                    // ajouter la requête "INSERT INTO"
                }
 
            } else {
                echo 'Echec de l\'upload !';
            }
        }
    }
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload Image</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <h1>Upload d'une image vers le Serveur</h1>
        <br><br>
        <form action="index.php" method="post" enctype="multipart/form-data">
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="titre">Titre de l'annonce</label>
                    <input type="text" class="form-control" id="titre" name="titre" value="<?php if (!empty($_POST["titre"])) {
                                                                                                echo $_POST["titre"];
                                                                                            } ?>">
                </div>

                <div class="col-md-12 mb-3">
                    <label for="message">Message de l'annonce</label>
                    <textarea class="form-control" name="message" id="message" rows="8"><?php if (!empty($_POST["message"])) {
                                                                                            echo $_POST["message"];
                                                                                        } ?></textarea>
                </div>
                <!-- value=1000000 exprimée en Octets, 1 000 000 Octets = 1 Mo -->
                <input type="hidden" name="MAX_FILE_SIZE" value="40000000" />

                <div class="col-md-12 mb-3">
                    <label for="image">Choisir une image</label>
                    <span class="erreur"><?php if (!empty($erreurs["image"])) {
                                                echo $erreurs["image"];
                                            } ?></span>
                    <input type="file" class="form-control" id="image" name="image">
                </div>

                <input class="btn btn-warning" type="submit" value="Ajouter" />
                <?php if (isset($_FILES['image'])) {
                    if (empty($erreurs["image"])) {
                        // récupération de l'id de l'annonce insérée
                        $id = $bdd->lastInsertId();

                ?>
                        <a href="vue_front.php?id=<?= $id ?>">Rendu sur le Front</a>
                <?php
                    }
                } ?>
            </div>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>
</body>

</html>